from django.shortcuts import render
from myapp.forms import my_review
from sklearn.linear_model import LogisticRegression
import pickle
import re

def emailClassification():
        filename = "myapp/emailUsingNB.pkl"
        #filename1 = "cv.pkl"
        f = open(filename,'rb')
        model = pickle.load(f)
        return model

def CV():
        filename = "myapp/cv.pkl"
        f = open(filename,'rb')
        cv = pickle.load(f)
        print(cv)
        return cv

def rest_review():
        filename = "myapp/review_model.pkl"
        f = open(filename,'rb')
        model = pickle.load(f)
        return model

def rest_cv():
        filename = "myapp/review_cv.pkl"
        f = open(filename,'rb')
        model = pickle.load(f)
        return model

def clean(x):
    #to remove html tag
    x = re.sub(r'<.*?>',' ',x)
    
    x= re.sub(r"can't",'can not',x)
    x= re.sub(r"don't",'do not',x)
    
    #change to mobile number
    x= re.sub(r'[\d-]{10,12}','mobno',x)
    
    #to remove punctuatin and numbers
    x = re.sub(r'[^A-Za-z]',' ',x)
    #to replace more than 1 space with 1 space
    x = re.sub(r'\s+',' ',x)
    
    #to convert into lower
    return x.lower()

def review_page(request): 
    form=my_review()
    if request.method=="POST":
        form=my_review(data=request.POST)
        if form.is_valid():
            data=form.cleaned_data['text']
            c_data= clean(data)
            cv=CV()
            txt=cv.transform([c_data]).toarray()
            fn = emailClassification()        
            prd=fn.predict(txt)
            target={0:"ham",1:"spam"}
            return render(request,'review.html',{'myform':form,'success':target[prd[0]]})
             
    return render(request,'review.html',{'myform':form})

def review_rest(request):     
        if request.method=="POST":
                review = request.POST['review']
                crv = clean(review)
                print(crv)

                cv = rest_cv()
                ctxt = cv.transform([crv]).toarray()
                model = rest_review()
                prediction = model.predict(ctxt)
                target = {0:'Negative Review',1:'Positive Review'}
                return render(request,'reataurant_reviews.html',{'success':target[prediction[0]]})
        return render(request,'reataurant_reviews.html')

